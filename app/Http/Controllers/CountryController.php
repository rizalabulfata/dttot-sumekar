<?php

namespace App\Http\Controllers;

use App\Models\Country;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests\UpdateCountryRequest;
use Barryvdh\Debugbar\Twig\Extension\Debug;

class CountryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Country $country)
    {

        // dd($data);
        // return DataTables::of($data)
        //     ->addColumn('Actions', function ($data) {
        //         return '<button type="button" class="btn btn-success btn-sm" id="getEditArticleData" data-id="' . $data->id . '">Edit</button>
        //             <button type="button" data-id="' . $data->id . '" data-toggle="modal" data-target="#DeleteArticleModal" class="btn btn-danger btn-sm" id="getDeleteId">Delete</button>';
        //     })
        //     ->rawColumns(['Actions'])
        //     ->make(true);

        // dd($data);
        // DataTables::of($data)
        //     ->addIndexColumn()
        //     ->addColumn('action', function ($data) {
        //         return view('layouts._action', [
        //             'model'      => $data->name,
        //             'urlShow'    => route('country.show', $data->id),
        //             'urlEdit'    => route('country.edit', $data->id),
        //             'urlDestroy' => route('country.destroy', $data->id)
        //         ]);
        //     })
        //     ->rawColumns(['action'])
        //     ->make(true);


        return view('country.country');
    }

    public function getArticles(Request $request, Country $country)
    {
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('country.add-country');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreCountryRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Country $country)
    {
        $validator = Validator::make($request->all(), [
            'code_country' => 'required|max:5',
            'name_country' => 'required',
        ]);


        if ($validator->fails()) {
            return redirect()->back()
                ->with('errorForm', $validator->errors()->getMessages())
                ->withInput();
        }

        try {
            $country->storeData($request->all());
            return redirect()->route('country.index')->with('success', 'Data berhasil disimpan');
        } catch (\Exception $e) {
            Debug::info($e->getMessage());
            return redirect()->back()->with('error', 'Error during the creation!');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Country  $country
     * @return \Illuminate\Http\Response
     */
    public function show(Country $country)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Country  $country
     * @return \Illuminate\Http\Response
     */
    public function edit(Country $country)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateCountryRequest  $request
     * @param  \App\Models\Country  $country
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateCountryRequest $request, Country $country)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Country  $country
     * @return \Illuminate\Http\Response
     */
    public function destroy(Country $country)
    {
        //
    }

    public function dataTables(Country $country)
    {
        $data = $country->getData();
        return DataTables::of($data)
            ->addColumn('Actions', function ($data) {
                return '<button type="button" class="btn btn-success btn-sm" id="getEditArticleData" data-id="' . $data->id . '">Edit</button>
                    <button type="button" data-id="' . $data->id . '" data-toggle="modal" data-target="#DeleteArticleModal" class="btn btn-danger btn-sm" id="getDeleteId">Delete</button>';
            })
            ->rawColumns(['Actions'])
            ->make(true);
    }
}