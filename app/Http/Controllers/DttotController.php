<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreDttotRequest;
use App\Http\Requests\UpdateDttotRequest;
use App\Models\Dttot;

class DttotController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('dttot.dttot');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dttot.add-dttot');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreDttotRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreDttotRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Dttot  $dttot
     * @return \Illuminate\Http\Response
     */
    public function show(Dttot $dttot)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Dttot  $dttot
     * @return \Illuminate\Http\Response
     */
    public function edit(Dttot $dttot)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateDttotRequest  $request
     * @param  \App\Models\Dttot  $dttot
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateDttotRequest $request, Dttot $dttot)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Dttot  $dttot
     * @return \Illuminate\Http\Response
     */
    public function destroy(Dttot $dttot)
    {
        //
    }
}
