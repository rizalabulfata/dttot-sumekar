<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    use HasFactory;
    protected $table = 'countries';
    protected $fillable = ['code_country', 'name_country'];
    protected $guarded = array();


    public function region()
    {
        return $this->hasMany(Country::class);
    }
    public function getData()
    {
        return static::orderBy('created_at', 'desc')->get();
    }

    public function storeData($input)
    {
        return static::create($input);
    }
}