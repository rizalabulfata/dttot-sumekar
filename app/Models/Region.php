<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Region extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     **/
    protected $table = 'regions';
    protected $fillable = [
        'country_id',
        'code_provinsi',
        'nama_provinsi',
        'code_kab',
        'nama_kab',
    ];
    protected $guarded = array();


    public function country()
    {
        return $this->belongsTo(Country::class);
    }

    public function getProvince()
    {
        return static::orderBy('created_at', 'desc')->get();
    }
}