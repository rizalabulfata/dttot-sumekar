<aside id="sidebar-wrapper">
    <div class="sidebar-brand">
        <a href="index.html">DTTOT BBS Sumenep</a>
        {{-- <img alt="image" src="../assets/img/logo/logo-bprs.png" class="rounded-circle"> --}}
    </div>
    <div class="sidebar-brand sidebar-brand-sm">
        <a href="index.html">St</a>
    </div>
    <ul class="sidebar-menu">
        <li class="menu-header">Dashboard</li>
        <li class="{{ request()->is('dashboard') ? 'active' : '' }}">
            <a class="nav-link" href="{{ route('dashboard') }}">
                <i class="fas fa-fire"></i>
                <span>Dashboard</span>
            </a>
        </li>
        <li class="menu-header">Master Data</li>
        <li class="nav-item  dropdown">
            <a href="#"
                class="nav-link active {{ request()->is('master') ? 'has-dropdown active' : 'has-dropdown' }}"
                data-toggle="dropdown"><i class="fas fa-columns"></i>
                Daftar & Data</a>
            <ul class="dropdown-menu">
                <li class="{{ request()->is('daftar-dttot') ? 'active' : '' }}">
                    <a class="nav-link {{ request()->is('daftar-dttot') ? 'active' : '' }}" href="{{route('daftar-dttot.index')}}">Daftar DTTOT</a>
                </li>
                <li class="{{ request()->is('country') ? 'active' : '' }}">
                    <a class="nav-link {{ request()->is('country') ? 'active' : '' }}"
                        href="{{ route('country.index') }}">Daftar Negara</a>
                </li>
                <li class="{{ request()->is('region') ? 'active' : '' }}">
                    <a class="nav-link {{ request()->is('region') ? 'active' : '' }}" href="{{route('region.index')}}">Daftar Region</a>
                </li>
            </ul>
        </li>

        <li class="menu-header">Data Nasabah</li>
        <li class="nav-item dropdown">
            <a href="#" class="nav-link has-dropdown"><i class="fas fa-th-large"></i>
                <span>Nasabah BBS</span></a>
            <ul class="dropdown-menu">
                <li><a class="nav-link" href="components-article.html">Nasabah Eligible</a></li>
                <li><a class="nav-link beep beep-sidebar" href="components-avatar.html">Nasabah Profesional</a></li>
                {{-- <li><a class="nav-link" href="components-chat-box.html">Chat Box</a></li>
                    <li><a class="nav-link beep beep-sidebar" href="components-empty-state.html">Empty State</a></li>
                    <li><a class="nav-link" href="components-gallery.html">Gallery</a></li>
                    <li><a class="nav-link beep beep-sidebar" href="components-hero.html">Hero</a></li>
                    <li><a class="nav-link" href="components-multiple-upload.html">Multiple Upload</a></li>
                    <li><a class="nav-link beep beep-sidebar" href="components-pricing.html">Pricing</a></li>
                    <li><a class="nav-link" href="components-statistic.html">Statistic</a></li>
                    <li><a class="nav-link" href="components-tab.html">Tab</a></li>
                    <li><a class="nav-link" href="components-table.html">Table</a></li>
                    <li><a class="nav-link" href="components-user.html">User</a></li>
                    <li><a class="nav-link beep beep-sidebar" href="components-wizard.html">Wizard</a></li> --}}
            </ul>
        </li>

    </ul>
</aside>
