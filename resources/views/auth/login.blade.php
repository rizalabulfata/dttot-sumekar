<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
    <title>Login &mdash; Stisla</title>

    <!-- General CSS Files -->
    <link rel="stylesheet" href="{{ asset('library/bootstrap/dist/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css"
        integrity="sha512-KfkfwYDsLkIlwQp6LFnl8zNdLGxu9YAA1QvwINks4PhcElQSvqcyVLLD9aMhXd13uQjoXtEKNosOWaZqXgel0g=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />

    <!-- CSS Libraries -->
    <link rel="stylesheet" href="{{ asset('library/bootstrap-social/bootstrap-social.css') }}">

    <!-- Template CSS -->
    <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/components.css') }}">
</head>

<body>
    <div id="app">
        <section class="section">
            <div class="d-flex flex-wrap align-items-stretch">
                <div class="col-lg-4 col-md-6 col-12 order-lg-1 min-vh-100 order-2 bg-white">
                    <div class="p-4 m-3">
                        <div class="text-center">
                            <img src="../assets/img/logo/bprs-logo.png" alt="logo" width="80"
                                class="shadow-light rounded-circle mb-5 mt-2 align-content-md-center">
                        </div>

                        <h4 class="text-dark font-weight-normal text-center">D T T O T<br> <span
                                class="font-weight-bold">BANK BBS Sumenep</span>
                        </h4>
                        <p class="text-muted">Sebelum Anda Melanjutkan, Anda perlu Masuk dan Daftar jika belum
                            mendapatkan
                            Akun</p>

                        <form method="POST" action="{{ route('login') }}" class="needs-validation" novalidate="">
                            @csrf
                            <div class="form-group">
                                <label for="email">Email</label>
                                <input id="email" type="email"
                                    class="form-control @error('email') is-invalid @enderror" name="email"
                                    tabindex="1" required autofocus>
                                @error('email')
                                    <div class="invalid-feedback">
                                        <strong>{{ $message }}</strong>
                                    </div>
                                @enderror
                            </div>

                            <div class="form-group">
                                <div class="d-block">
                                    <label for="password" class="control-label">Password</label>
                                </div>
                                <input id="password" type="password"
                                    class="form-control @error('password') is-invalid @enderror" name="password"
                                    tabindex="2" required autocomplete="current-password">

                                @error('password')
                                    <div class="invalid-feedback">
                                        <strong>{{ $message }}</strong>
                                    </div>
                                @enderror
                            </div>

                            <div class="form-group">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" name="remember" class="custom-control-input" tabindex="3"
                                        id="remember-me" {{ old('remember') ? 'checked' : '' }}>
                                    <label class="custom-control-label" for="remember-me">Ingat Saya</label>
                                </div>
                            </div>

                            <div class="form-group text-right">
                                @if (Route::has('password.request'))
                                    <a href="{{ route('password.request') }}" class="float-left mt-3">
                                        Lupa Password?
                                    </a>
                                @endif

                                <button type="submit" class="btn btn-success btn-lg btn-icon icon-right"
                                    tabindex="4">
                                    Masuk
                                </button>
                            </div>

                            <div class="mt-5 text-center">
                                Belum punya Akun? <a href="{{ route('register') }}">Daftar</a>
                            </div>
                        </form>

                        <div class="text-center mt-5 text-small">
                            Copyright &copy; Najib 💙
                            {{-- <div class="mt-2">
                                <a href="#">Privacy Policy</a>
                                <div class="bullet"></div>
                                <a href="#">Terms of Service</a>
                            </div> --}}
                        </div>
                    </div>
                </div>
                <div class="col-lg-8 col-12 order-lg-2 order-1 min-vh-100 background-walk-y position-relative overlay-gradient-bottom"
                    data-background="../assets/img/logo/sumenep.jpg">
                    <div class="absolute-bottom-left index-2">
                        <div class="text-light p-5 pb-2 text-center">
                            <div class="mb-5 pb-3">
                                <h2 class="mb-2 display-4 font-weight-bold">Sistem Informasi dan Validasi
                                    Nasabah
                                </h2>
                                <h5
                                    class="font-weight-normal
                                    text-muted-transparent">
                                    <b>Bank BBS Sumekar Sumenep.</b><a class="text-light bb" target="_blank"
                                        href="https://www.bhaktisumekar.co.id/v2/"> Klik disini</a>
                                </h5>
                                <div>
                                    <small>
                                        Jl. Trunojoyo No.137, Karangrawa, Bangselok, Kec. Kota Sumenep, Kabupaten
                                        Sumenep, Jawa Timur 69417
                                    </small>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <!-- General JS Scripts -->
    <script src="{{ asset('library/jquery/dist/jquery.min.js') }}"></script>
    <script src="{{ asset('library/popper.js/dist/umd/popper.js') }}"></script>
    <script src="{{ asset('library/tooltip.js/dist/umd/tooltip.js') }}"></script>
    <script src="{{ asset('library/bootstrap/dist/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('library/jquery.nicescroll/dist/jquery.nicescroll.min.js') }}"></script>
    <script src="{{ asset('library/moment/min/moment.min.js') }}"></script>
    <script src="{{ asset('assets/js/stisla.js') }}"></script>

    <!-- JS Libraies -->

    <!-- Page Specific JS File -->

    <!-- Template JS File -->
    <script src="{{ asset('assets/js/scripts.js') }}"></script>
    <script src="{{ asset('assets/js/custom.js') }}"></script>
</body>

</html>
