@extends('layouts.master')
@section('title', 'Daftar Nasabah')

@push('style')
{{--    <link rel="stylesheet" href="{{ asset('library/selectric/public/selectric.css') }}">--}}
{{--    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css">--}}
@endpush

@section('content')
<section class="section">
    <div class="section-header">
        <h1>Daftar Nasabah</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
            <div class="breadcrumb-item"><a href="#">Master Data</a></div>
            <div class="breadcrumb-item">Daftar Nasabah</div>
        </div>
    </div>
    <div class="section-body">
        <div class="row">
            <div class="col-12">
                <div class="card mb-0">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="section-header-button">
                                    <a href="{{ route('daftar-dttot.create') }}" class="btn btn-primary">Tambah</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {{-- @if ($message = Session::get('success'))
                            <div class="alert alert-success">
                                <p>{{ $message }}</p>
            </div>
            @endif --}}
        </div>
    </div>
    </div>
    <div class="row mt-3">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-striped datatable table-hover table-responsive">
                            <thead>
                                <tr>
                                    <th>Nama</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <th>fina</th>
                                    <td>Mark</td>
                                    <td>Otto</td>
                                </tr>
                                <tr>
                                    <th>Ahmad</th>
                                    <td>Mark</td>
                                    <td></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
</section>
@endsection

@push('script') @endpush