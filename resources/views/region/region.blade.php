@extends('layouts.master')
@section('title', 'Data Provinsi dan Kabupaten')
@push('style')
    <link rel="stylesheet" href="{{ asset('library/selectric/public/selectric.css') }}">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css">
@endpush
@section('content')
    <section class="section">
        <div class="section-header">
            <h1>Daftar Code dan Nama Negara</h1>
            <div class="section-header-breadcrumb">
                <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
                <div class="breadcrumb-item"><a href="#">Master Data</a></div>
                <div class="breadcrumb-item">Nama Negara</div>
            </div>
        </div>
        <div class="section-body">
            <div class="row">
                <div class="col-12">
                    <div class="card mb-0">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="section-header-button">
                                        <a href="{{ route('region.create') }}" class="btn btn-primary">Tambah</a>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class=" text-right">
                                        <a href="#" class="btn btn-secondary">CSV</a>
                                        <a href="#" class="btn btn-danger">Excel</a>
                                        <a href="#" class="btn btn-success">PDF</a>
                                        <a href="#" class="btn btn-light">Upload</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {{-- @if ($message = Session::get('success'))
                            <div class="alert alert-success">
                                <p>{{ $message }}</p>
                            </div>
                        @endif --}}
                    </div>
                </div>
            </div>
            <div class="row mt-3">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-striped datatable">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Code Provinsi</th>
                                            <th>Nama Provinsi</th>
                                            <th>Code Kabupaten/Kota</th>
                                            <th>Nama Kabupaten/Kota</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection
@push('scripts')
    <!-- JS Libraies -->
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
    {{-- <script src="{{ asset('library/datatables/media/js/jquery.dataTables.min.js') }}"></script> --}}
    {{-- <script src="{{ asset('library/jquery-ui-dist/jquery-ui.min.js') }}"></script> --}}
    <!-- Page Specific JS File -->
    <script src="{{ asset('assets/js/page/modules-datatables.js') }}"></script>
    <!-- General JS Scripts -->

    <script type="text/javascript">
        $(document).ready(function() {
            var dataTable = $('.datatable').DataTable({
                processing: true,
                serverSide: true,
                autoWidth: false,
                // pageLength: 5,
                // scrollX: true,
                "order": [
                    [0, "asc"]
                ],
                ajax: '{{ route('region-dataTables') }}',
                type: "POST",
                columns: [{
                        data: null,
                        orderable: false,
                        searchable: false,
                        render: function(data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        }
                    },
                    {
                        data: 'code_provinsi',
                        name: 'code_provinsi',
                        // className: 'text-center'
                    }, {
                        data: 'nama_provinsi',
                        name: 'nama_provinsi'
                    },
                    {
                        data: 'code_kab',
                        name: 'code_kab'
                    },
                    {
                        data: 'nama_kab',
                        name: 'nama_kab'
                    },
                    {
                        data: 'Actions',
                        name: 'Actions',
                        orderable: false,
                        serachable: false,
                        sClass: 'text-center'
                    },
                ]
            });
        });
    </script>
@endpush
