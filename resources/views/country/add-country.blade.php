@extends('layouts.master')
@section('title', 'Data Negara')
@push('style')
    <link rel="stylesheet" href="{{ asset('library/selectric/public/selectric.css') }}">
@endpush
@section('content')
    <section class="section">
        <div class="section-header">
            <h1>Tambah Code dan Nama Negara</h1>
            <div class="section-header-breadcrumb">
                <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
                <div class="breadcrumb-item"><a href="#">Master Data</a></div>
                <div class="breadcrumb-item"><a href="#">Daftar Negara</a></div>
                <div class="breadcrumb-item">Tambah</div>
            </div>
        </div>
        <div class="section-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="card mb-0">
                        <div class="card-header">
                            <h4>Form Input</h4>
                        </div>
                        <div class="card-body">
                            <form action="{{ route('country.store') }}" method="POST">
                                @csrf
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Kode Negara <sup class="text-danger">*</sup></label>
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <div class="input-group-text">
                                                        <i class="fas fa-flag"></i>
                                                    </div>
                                                </div>
                                                <input type="text"
                                                    class="form-control @error('code_country') is-invalid @enderror"
                                                    data-indicator="pwindicator" name="code_country" required="" autofocus
                                                    {{ old('code_country') }}>
                                                @error('code_country')
                                                    <div class="invalid-feedback">{{ $message }}</div>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Nama Negara <sup class="text-danger">*</sup></label>
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <div class="input-group-text">
                                                        <i class="fas fa-globe"></i>
                                                    </div>
                                                </div>
                                                <input type="text"
                                                    class="form-control @error('name_country') is-invalid @enderror"
                                                    name="name_country" required="" value="{{ old('title') }}">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="breadcrumb-item">
                                    <span class=""><sup class="text-danger"><strong>*</strong></sup> Wajib
                                        diisi</span>
                                </div>
                                <div class="card-footer text-right">
                                    <button class="btn btn-primary"><i class="fas fa-save mr-2"></i>Simpan</button>
                                    <a href="{{route('country.index')}}" class="btn btn-secondary"><i class="far fa-arrow-alt-circle-left mr-2"></i>Kembali</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>

@endsection
@push('scripts')

@endpush
