<?php

use App\Http\Controllers\CountryController;
use App\Http\Controllers\RegionController;
use App\Http\Controllers\DttotController;
use \App\Http\Controllers\HomeController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
})->middleware('auth');

Route::middleware(['auth', 'web'])->group(function () {
    Route::get('/dashboard', [HomeController::class, 'index'])->name('dashboard');

    //    Router Country
    Route::resource('country', CountryController::class);
    Route::get('/dataTables', [CountryController::class, 'dataTables'])->name('country-dataTables');


    //    Router Region
    Route::get('/region/dataTables', [RegionController::class, 'dataTablesRegion'])->name('region-dataTables');
    Route::resource('region', RegionController::class);

    //    Router DTTOT
    Route::resource('daftar-dttot', DttotController::class);
    //    Route::get('dataTables', [DttotController::class, 'dataTables'])->name('dttot-dataTables');

});

Auth::routes();



// Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
